<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('test', function () {
    /** @var \App\Models\PatientPassport $passport */
    $passport = \App\Models\PatientPassport::find(1);
    $passport->setTranslation('address', 'en', 'Moskva, proezd Dezhneva, 29k1, kv. 104');
    $passport->setTranslation('issuer', 'en', 'FMS 77220');
    $passport->save();
})->purpose('Display an inspiring quote');

Artisan::command('test:1', function () {
    /** @var \App\Models\Gemotest\GemotestResult $result */
    $result = \App\Models\Gemotest\GemotestResult::find(1);
    $result->sendEmail('ping@baev.dev');
})->purpose('Display an inspiring quote');

