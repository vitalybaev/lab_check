<?php

use App\Http\Controllers\GemotestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain('gemotest-online.ru')->group(function () {
    /**
     * Gemotest
     */
    Route::get('/gemotest/pdf/{resultId}', [GemotestController::class, 'getPdfHtml']);
    Route::get('/gemotest/download-pdf/{resultId}', [GemotestController::class, 'getPdf']);
    Route::get('/gemotest/send-email/{resultId}', [GemotestController::class, 'sendEmail']);

    /**
     * Public
     */
    Route::get('/covidcheck', [GemotestController::class, 'verify']);
    Route::get('/covidcheck', [GemotestController::class, 'verify']);

    Route::fallback(function () {
        return redirect()->to('https://gemotest.ru/');
    });
});
