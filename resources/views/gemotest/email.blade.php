<?php /** @var \App\Models\Gemotest\GemotestResult $result */ ?>

<p>Здравствуйте! Благодарим вас за доверие нашей лаборатории!</p>

<p>Мы прикрепили результаты исследований к этому письму в виде PDF-файла. Если файл не открывается, установите бесплатную программу для просмотра PDF, перейдя по ссылке:</p>

<ul>
    <li><a href="https://get2.adobe.com/ru/reader/">ПК</a></li>
    <li>
        <a href="https://play.google.com/store/apps/details?id=com.adobe.reader&promoid=GMCWYC2L&mv=other&hl=ru&_branch_match_id=848084745902000762">Android</a></li>
    <li>
        <a href="https://apps.apple.com/ru/app/adobe-reader/id469337564?mv=other&mv=other&promoid=GHMVYGSM&_branch_match_id=848084745902000762">iOS</a>
    </li>
</ul>

<p>Есть вопросы? Мы будем рады вам помочь! Наши контакты для связи:</p>

<p>Мессенджеры:</p>

<ul>
    <li><a href="https://api.whatsapp.com/send/?phone=79255501313&text&app_absent=0">WhatsApp</a></li>
    <li><a href="https://viber.com">Viber</a></li>
    <li><a href="https://t.me/gemotest_labbot">Telegram</a></li>
</ul>

<p>Социальные сети:</p>

<ul>
    <li><a href="https://vk.com/gemotestlab">ВКонтакте</a></li>
    <li><a href="https://www.facebook.com/gemotestlab/">Facebook</a></li>
    <li><a href="https://www.instagram.com/gemotest.lab/">Instagram</a></li>
    <li><a href="https://ok.ru/group/55825532125349">Одноклассники</a></li>
</ul>

<p>Телефоны:</p>

<ul>
    <li>8(495)532-13-13</li>
    <li>8(800)550-13-13</li>
</ul>

<p>Как наш клиент, вы являетесь участником бонусной программы – с помощью бонусов можно оплатить до 25% стоимости заказа.</p>

<p style="font-style: italic;">Письмо отправлено системой автоматической рассылки – отвечать на него не нужно.</p>

<p><a href="https://gemotest.ru">www.gemotest.ru</a></p>
