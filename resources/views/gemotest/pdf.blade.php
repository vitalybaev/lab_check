<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
    <style>
        *, html, body {
            font-family: 'Rubik', sans-serif;
        }

        table {
            width: 100%;
        }

        .table--with-border {
            border-collapse: collapse;
            font-size: 11px;
        }

        .table--with-border td,
        .table--with-border th {
            font-weight: normal;
            padding: 5px;
            border: 1.5px #000 solid;
        }

        .table--with-border td {
            padding: 5px 10px;
        }

        .table--with-border th {
            text-align: center;
        }

        .value-table td {
            padding: 3px 2px;
        }

        .label-cell {
            font-size: 11px;
        }

        .label-cell--bold {
            font-weight: bold;
        }

        .value-cell {
            font-size: 11px;
            border-bottom: 1px #000 solid;
        }
    </style>
</head>
<?php /** @var \App\Models\Gemotest\GemotestResult $result */ ?>
<body>
    <div style="display: flex; flex-direction: column">
        <div>
            <table>
                <tr>
                    <td>
                        <img src="{{ base64_image('resources/gemotest/pdf/logo_ru.jpg', 'jpeg') }}" width="447" height="75" alt="">
                        <div style="font-size: 7px; line-height: 1.4">
                            ООО «Лаборатория Гемотест»<br />
                            107045, г. Москва, Рождественский б-р, д. 21, стр. 2<br />
                            тел.: +7 495 532 13 13, 8 800 550 13 13<br />
                            www.gemotest.ru<br />
                            ЛО-50-01-012467 от 24.11.2020 выдана Министерством здравоохранения Московской области
                        </div>
                    </td>
                    <td style="text-align: right">
                        <div style="width: 130px; display: inline-block; text-align: center; padding: 0 5px 0 5px">
                            <img src="{{ qr_code($result->getVerifyUrl()) }}" width="110" height="110" alt="">

                            <div style="font-size: 7px; line-height: 1.4; margin-top: 5px;">
                                Отсканируйте QR-код для проверки подлинности сертификата.
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

            <div style="font-weight: 600; margin-top: 15px; font-size: 30px; text-align: center; text-transform: uppercase;">Сертификат</div>

            <table class="value-table" style="margin-top: 10px;">
                <tr>
                    <td class="label-cell label-cell--bold" style="width: 80px;">
                        № заказа:
                    </td>
                    <td class="value-cell">
                        {{ $result->result_id }}
                    </td>
                </tr>
            </table>

            <div style="margin-top: 15px; font-weight: bold; font-size: 11px; margin-left: 5px;">Документ (паспорт, св. о рождении)</div>

            <table class="value-table" style="margin-top: 10px;">
                <tr>
                    <td class="label-cell" style="width: 80px;">
                        ФИО:
                    </td>
                    <td class="value-cell">
                        {{ $result->patient->getTranslation('last_name', 'ru') }} {{ $result->patient->getTranslation('first_name', 'ru') }} {{ $result->patient->getTranslation('patronymic_name', 'ru') }}
                    </td>

                    <td class="label-cell" style="width: 100px;">
                        Дата рождения:
                    </td>
                    <td class="value-cell">
                        {{ \Carbon\Carbon::parse($result->patient->birth_date)->format('d.m.Y') }}
                    </td>
                </tr>
            </table>

            <table class="value-table" style="margin-top: 6px;">
                <tr>
                    <td class="label-cell" style="width: 80px;">
                        Серия, номер:
                    </td>
                    <td class="value-cell">
                        {{ $result->patientDocument->series }}{{ $result->patientDocument->number }}
                    </td>

                    <td class="label-cell" style="width: 40px; padding-left: 10px;">
                        Выдан:
                    </td>
                    <td class="value-cell">
                        {{ $result->patientDocument->getTranslation('issuer', 'ru') }}
                    </td>
                </tr>
            </table>

            <table class="value-table" style="margin-top: 6px;">
                <tr>
                    <td class="label-cell" style="width: 120px;">
                        Адрес проживания:
                    </td>
                    <td class="value-cell">
                        {{ $result->patientDocument->getTranslation('address', 'ru') }}
                    </td>
                </tr>
            </table>

            <div style="margin-top: 25px; font-size: 11px; margin-left: 5px;">
                О результатах лабораторного теста на коронавирусную инфекцию SARS-CoV-2
            </div>

            <table class="table--with-border" style="margin-top: 15px;">
                <tr>
                    <th>
                        Тест
                    </th>
                    <th>
                        Дата взятия<br />
                        биоматериала<br />
                        (московское время)
                    </th>
                    <th>
                        Дата выполнения<br />
                        (московское время)
                    </th>
                    <th>
                        Результат тестирования
                    </th>
                </tr>
                <tr>
                    <td>
                        Коронавирус, РНК (SARS-CoV-2, ПЦР)<br />
                        мазок, кач.
                    </td>
                    <td>
                        <div style="font-size: 10px; white-space: nowrap;">{{ \Carbon\Carbon::parse($result->analysis_taken_at)->format('d.m.Y H:i:s') }}</div>
                    </td>
                    <td>
                        <div style="font-size: 10px; white-space: nowrap;">{{ \Carbon\Carbon::parse($result->analysis_ready_at)->format('d.m.Y H:i:s') }}</div>
                    </td>
                    <td>
                        РНК не обнаружена
                    </td>
                </tr>
            </table>

            <div style="margin-top: 15px; font-size: 11px; margin-left: 5px;">
                Качественное определение РНК коронавируса SARS-CoV-2 (возбудителя COVID-19) в мазках из носоглотки и ротоглотки выполняется методом ПЦР в режиме реального времени.
            </div>

            <div style="margin-top: 25px; font-size: 11px; margin-left: 5px; font-weight: bold;">
                Информируем, что любая подделка документа запрещена и преследуется по закону.
            </div>
        </div>

        <div style="margin-top: calc(50% - 40px);">
            <table>
                <tr>
                    <td>
                        <img src="{{ base64_image('resources/gemotest/pdf/stamp_square.jpg', 'jpeg') }}" width="147" height="76" alt="">
                    </td>
                    <td style="text-align: right">
                        <img src="{{ base64_image('resources/gemotest/pdf/stamp_circle.jpg', 'jpeg') }}" width="160" height="160" alt="">
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!-- English Section -->

    <div style="break-after: page;">
        <div>
            <table>
                <tr>
                    <td>
                        <img src="{{ base64_image('resources/gemotest/pdf/logo_en.jpg', 'jpeg') }}" width="445" height="75" alt="">
                        <div style="font-size: 7px; line-height: 1.4">
                            Laboratory Gemotest LLC<br/>
                            21 Rozhdestvensky b-d, building 2 Moscow, 107045<br/>
                            p.: +7 495 532 13 13, 8 800 550 13 13<br/>
                            www.gemotest.ru<br/>
                            LO-50-01-012467
                        </div>
                    </td>
                    <td style="text-align: right">
                        <div style="width: 130px; display: inline-block; text-align: center; padding: 0 5px 0 5px">
                            <img src="{{ qr_code($result->getVerifyUrl()) }}" width="110" height="110" alt="">

                            <div style="font-size: 7px; line-height: 1.4; margin-top: 5px;">
                                Scan QR code above to verify the certificate.
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

            <div style="font-weight: 600; margin-top: 15px; font-size: 30px; text-align: center; text-transform: uppercase;">
                Certificate
            </div>

            <table class="value-table" style="margin-top: 10px;">
                <tr>
                    <td class="label-cell label-cell--bold" style="width: 80px;">
                        Order:
                    </td>
                    <td class="value-cell">
                        {{ $result->result_id }}
                    </td>
                </tr>
            </table>

            <div style="margin-top: 15px; font-weight: bold; font-size: 11px; margin-left: 5px;">
                Passport (ID)
            </div>

            <table class="value-table" style="margin-top: 10px;">
                <tr>
                    <td class="label-cell" style="width: 80px;">
                        Name:
                    </td>
                    <td class="value-cell">
                        {{ $result->patient->getTranslation('last_name', 'en') }} {{ $result->patient->getTranslation('first_name', 'en') }} {{ $result->patient->getTranslation('patronymic_name', 'en') }}
                    </td>

                    <td class="label-cell" style="width: 100px;">
                        Date of birth:
                    </td>
                    <td class="value-cell">
                        {{ \Carbon\Carbon::parse($result->patient->birth_date)->format('d.m.Y') }}
                    </td>
                </tr>
            </table>

            <table class="value-table" style="margin-top: 6px;">
                <tr>
                    <td class="label-cell" style="width: 80px;">
                        Number:
                    </td>
                    <td class="value-cell">
                        {{ $result->patientDocument->series }}{{ $result->patientDocument->number }}
                    </td>

                    <td class="label-cell" style="width: 40px; padding-left: 10px;">
                        Issued:
                    </td>
                    <td class="value-cell">
                        {{ $result->patientDocument->getTranslation('issuer', 'en') }}
                    </td>
                </tr>
            </table>

            <table class="value-table" style="margin-top: 6px;">
                <tr>
                    <td class="label-cell" style="width: 80px;">
                        Address:
                    </td>
                    <td class="value-cell">
                        {{ $result->patientDocument->getTranslation('address', 'en') }}
                    </td>
                </tr>
            </table>

            <div style="margin-top: 25px; font-size: 11px; margin-left: 5px;">
                About the results of laboratory tests for coronavirus infection SARS-CoV-2
            </div>

            <table class="table--with-border" style="margin-top: 15px;">
                <tr>
                    <th>
                        Name of research
                    </th>
                    <th>
                        Date of biomaterial taken<br />
                        (moscow time)
                    </th>
                    <th>
                        Date of completion<br />
                        (moscow time)
                    </th>
                    <th>
                        Test result
                    </th>
                </tr>
                <tr>
                    <td>
                        Coronavirus, RNA (SARS-CoV-2, Real-<br />
                        time PCR) smear, qualitative
                    </td>
                    <td>
                        {{ \Carbon\Carbon::parse($result->analysis_taken_at)->format('d.m.Y H:i:s') }}
                    </td>
                    <td>
                        {{ \Carbon\Carbon::parse($result->analysis_ready_at)->format('d.m.Y H:i:s') }}
                    </td>
                    <td>
                        Negative
                    </td>
                </tr>
            </table>

            <div style="margin-top: 15px; font-size: 11px; margin-left: 5px;">
                Qualitative determination of the RNA of the SARS-CoV-2 coronavirus (the causative agent of COVID-19) in smears from the nasopharynx and oropharynx is performed by real-time PCR.
            </div>

            <div style="margin-top: 25px; font-size: 11px; margin-left: 5px; font-weight: bold;">
                Forgery of documents is punishable by law.
            </div>
        </div>

        <div style="margin-top: calc(50% - 40px);">
            <table>
                <tr>
                    <td>
                        <img src="{{ base64_image('resources/gemotest/pdf/stamp_square.jpg', 'jpeg') }}" width="147" height="76" alt="">
                    </td>
                    <td style="text-align: right">
                        <img src="{{ base64_image('resources/gemotest/pdf/stamp_circle.jpg', 'jpeg') }}" width="160" height="160" alt="">
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
