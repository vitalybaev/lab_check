<?php

namespace App\Http\Controllers;

use App\Models\Gemotest\GemotestResult;
use Illuminate\Http\Request;
use Spatie\Browsershot\Browsershot;

class GemotestController extends Controller
{
    public function getPdf($resultId)
    {
        $result = GemotestResult::findOrFail($resultId);

        return redirect()->to($result->getPdfPublicUrl(false));
    }

    public function getPdfHtml($resultId)
    {
        $result = GemotestResult::findOrFail($resultId);
        return view('gemotest.pdf', ['result' => $result]);
    }

    public function verify(Request $request)
    {
        $result = GemotestResult::where('result_uid', $request->input('key'))->first();
        if (!$result) {
            return redirect()->to('https://gemotest.ru');
        }

        return view('gemotest.verify', ['result' => $result]);
    }

    public function sendEmail($resultId)
    {
        $result = GemotestResult::findOrFail($resultId);
        $result->sendEmail('ping@baev.dev');
    }
}
