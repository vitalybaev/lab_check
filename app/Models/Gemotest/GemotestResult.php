<?php

namespace App\Models\Gemotest;

use App\Mail\GemotestResultMail;
use App\Models\Patient;
use App\Models\PatientPassport;
use Illuminate\Database\Eloquent\Model;
use Spatie\Browsershot\Browsershot;
use Storage;

/**
 * App\Models\Gemotest\GemotestResult
 *
 * @property int $id
 * @property int $patient_id
 * @property int $patient_document_id
 * @property string $result_uid
 * @property string $result_id
 * @property string $analysis_taken_at
 * @property string $analysis_ready_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult query()
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult whereAnalysisReadyAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult whereAnalysisTakenAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult wherePatientDocumentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult whereResultId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult whereResultUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GemotestResult whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read Patient|null $patient
 * @property-read PatientPassport|null $patientDocument
 */
class GemotestResult extends Model
{
    protected $table = 'results_gemotest';

    public function getVerifyUrl()
    {
        return env('GEMOTEST_URL') . "/covidcheck/?key={$this->result_uid}";
    }

    public function patient()
    {
        return $this->hasOne(Patient::class, 'id', 'patient_id');
    }

    public function patientDocument()
    {
        return $this->hasOne(PatientPassport::class, 'id', 'patient_document_id');
    }

    public function generatePdf($path)
    {
        $pdf = Browsershot::html(view('gemotest.pdf', ['result' => $this])->render())
            ->setNodeBinary(env('NODE_PATH'))
            ->setNpmBinary(env('NPM_PATH'))
            ->format('A4')
            ->addChromiumArguments([
                'no-sandbox',
            ])
            ->margins(10, 15, 10, 15)
            ->pdf();

        Storage::put($path, $pdf);
    }

    /**
     * @param bool $force
     * @return string
     */
    public function getPdfPublicUrl($force = true)
    {
        $filePath = $this->getPdfLocalPath();

        if ($force || !Storage::exists($filePath)) {
            $this->generatePdf($filePath);
        }

        return Storage::url($filePath);
    }

    /**
     * @param bool $force
     * @return string
     */
    public function getPdfContent($force = true)
    {
        $filePath = $this->getPdfLocalPath();

        if ($force || !Storage::exists($filePath)) {
            $this->generatePdf($filePath);
        }

        return Storage::get($filePath);
    }

    /**
     * @return string
     */
    protected function getPdfLocalPath(): string
    {
        return "gemotest/" . $this->result_uid . ".pdf";
    }

    public function sendEmail($emailAddress)
    {
        $this->getPdfPublicUrl(false);
        $mail = new GemotestResultMail($this);
        $mail->attachFromStorage($this->getPdfLocalPath(), $this->result_id . '_' . $this->result_uid . '.pdf', [
            'mime' => 'application/pdf'
        ]);

        \Mail::to($emailAddress)->send($mail);
    }
}
