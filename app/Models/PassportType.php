<?php

namespace App\Models;

class PassportType
{
    const RUSSIAN_INTERNAL = 1;
    const RUSSIAN_INTERNATIONAL = 2;
}
