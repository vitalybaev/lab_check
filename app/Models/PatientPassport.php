<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\PatientPassport
 *
 * @property int $id
 * @property string|null $name
 * @property int $document_type
 * @property string|null $series
 * @property string|null $number
 * @property string|null $issued_at
 * @property string|null $expires_at
 * @property array|null $issuer
 * @property array|null $address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read array $translations
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport query()
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport whereDocumentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport whereIssuedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport whereIssuer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport whereSeries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $patient_id
 * @method static \Illuminate\Database\Eloquent\Builder|PatientPassport wherePatientId($value)
 */
class PatientPassport extends Model
{
    use HasTranslations;

    protected $table = 'patients_passports';

    public $translatable = ['issuer', 'address'];
}
