<?php

use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

function base64_image($path, $contentType) {
    $image = public_path($path);
    $imageContent = base64_encode(file_get_contents($image));
    return base64_content($imageContent, $contentType);
}

function base64_content($content, $contentType) {
    return "data:image/{$contentType};base64, $content";
}

function qr_code($data) {
    $writer = new PngWriter();

    $qrCode = QrCode::create($data)
        ->setEncoding(new Encoding('UTF-8'))
        ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
        ->setSize(300)
        ->setMargin(10)
        ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
        ->setForegroundColor(new Color(0, 0, 0))
        ->setBackgroundColor(new Color(255, 255, 255));

    $result = $writer->write($qrCode);

    return $result->getDataUri();
}

function gemotest_star_string($string) {
    if (mb_strlen($string) <= 2) {
        return $string;
    }

    return mb_substr($string, 0, 1) . '***' . mb_substr($string, mb_strlen($string) - 1, 1);
}

function gemotest_name_and_initials(\App\Models\Patient $patient, $locale) {
    $parts = [gemotest_star_string($patient->getTranslation('last_name', $locale))];
    $firstName = $patient->getTranslation('first_name', $locale);
    $patronymicName = $patient->getTranslation('patronymic_name', $locale);
    if ($firstName) {
        $parts[] = mb_substr($firstName, 0, 1) . '.';
    }
    if ($patronymicName) {
        $parts[] = mb_substr($patronymicName, 0, 1) . '.';
    }

    return implode(' ', $parts);
}

function month_name_short_ru($monthNumber) {
    switch ($monthNumber) {
        case 1:
            return 'янв';
        case 2:
            return 'фев';
        case 3:
            return 'мар';
        case 4:
            return 'апр';
        case 5:
            return 'мая';
        case 6:
            return 'июн';
        case 7:
            return 'июл';
        case 8:
            return 'авг';
        case 9:
            return 'сен';
        case 10:
            return 'окт';
        case 11:
            return 'ноя';
        case 12:
            return 'дек';
        default:
            return '';
    }
}

function month_name_short_en($monthNumber) {
    switch ($monthNumber) {
        case 1:
            return 'Jan';
        case 2:
            return 'Feb';
        case 3:
            return 'Mar';
        case 4:
            return 'Apr';
        case 5:
            return 'May';
        case 6:
            return 'Jun';
        case 7:
            return 'Jul';
        case 8:
            return 'Aug';
        case 9:
            return 'Sep';
        case 10:
            return 'Oct';
        case 11:
            return 'Nov';
        case 12:
            return 'Dec';
        default:
            return '';
    }
}
