<?php

namespace App\Mail;

use App\Models\Gemotest\GemotestResult;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GemotestResultMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var GemotestResult
     */
    public $result;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(GemotestResult $result)
    {
        $this->result = $result;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('gemotest.email')
            ->from('info@gemotest.ru', 'Лаборатория Гемотест')
            ->subject("Результаты анализов. Лаборатория Гемотест. N заказа {$this->result->result_id}, {$this->result->patient->last_name}, {$this->result->patient->first_name} {$this->result->patient->patronymic_name}");
    }
}
