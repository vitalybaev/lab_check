<?php

namespace App\Console\Commands;

use App\Models\Patient;
use App\Models\PatientPassport;
use Illuminate\Console\Command;

class CreatePatientPassportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:patient-passport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создает новый паспорт пациента';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $patientId = $this->ask('ID пациента');
        $name = $this->ask('Название документа');

        $type = $this->choice(
            'Тип документа',
            ['1', '2']
        );

        $series = $this->ask('Серия документа');
        $number = $this->ask('Номер документа');

        $issuedAt = $this->ask('Дата выдачи YYYY-MM-DD');
        $expiresAt = $this->ask('Срок окончания YYYY-MM-DD');

        $issuerRu = $this->ask('Кем выдан (RU)');
        $addressRu = $this->ask('Адрес проживания (RU)');
        $issuerEn = $this->ask('Кем выдан (EN)');
        $addressEn = $this->ask('Адрес проживания (EN)');

        $patient = Patient::findOrFail($patientId);

        $patientDocument = new PatientPassport();
        $patientDocument->patient_id = $patient->id;
        $patientDocument->name = $name;
        $patientDocument->document_type = (int) $type;
        $patientDocument->series = $series;
        $patientDocument->number = $number;
        $patientDocument->issued_at = $issuedAt;
        if ($expiresAt) {
            $patientDocument->expires_at = $expiresAt;
        }
        $patientDocument->setTranslation('issuer', 'ru', $issuerRu);
        $patientDocument->setTranslation('issuer', 'en', $issuerEn);
        $patientDocument->setTranslation('address', 'ru', $addressRu);
        $patientDocument->setTranslation('address', 'en', $addressEn);
        $patientDocument->save();

        $this->info('Документ успешно создан');

        return 0;
    }
}
