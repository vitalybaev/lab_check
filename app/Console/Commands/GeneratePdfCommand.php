<?php

namespace App\Console\Commands;

use App\Models\Gemotest\GemotestResult;
use Illuminate\Console\Command;

class GeneratePdfCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gemotest:generate-pdf {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерирует PDF для указанного заказа';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $resultId = $this->argument('id');

        $gemotestResult = GemotestResult::findOrFail($resultId);
        $gemotestResult->getPdfPublicUrl(true);

        return 0;
    }
}
