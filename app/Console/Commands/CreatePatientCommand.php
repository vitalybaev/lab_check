<?php

namespace App\Console\Commands;

use App\Models\Patient;
use Illuminate\Console\Command;

class CreatePatientCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:patient';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создает нового пациента';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $firstNameRu = $this->ask('Имя пациента (RU)');
        $lastNameRu = $this->ask('Фамилия пациента (RU)');
        $patronymicNameRu = $this->ask('Отчество пациента (RU)');

        $firstNameEn = $this->ask('Имя пациента (EN)');
        $lastNameEn = $this->ask('Фамилия пациента (EN)');
        $patronymicNameEn = $this->ask('Отчество пациента (EN)');

        $gender = $this->choice(
            'Пол',
            ['male', 'female']
        );

        $birthDate = $this->ask('Дата рождения YYYY-MM-DD');

        $patient = new Patient();
        $patient->setTranslation('first_name', 'ru', $firstNameRu);
        $patient->setTranslation('first_name', 'en', $firstNameEn);
        $patient->setTranslation('last_name', 'ru', $lastNameRu);
        $patient->setTranslation('last_name', 'en', $lastNameEn);
        $patient->setTranslation('patronymic_name', 'ru', $patronymicNameRu);
        $patient->setTranslation('patronymic_name', 'en', $patronymicNameEn);
        $patient->gender = $gender;
        $patient->birth_date = $birthDate;
        $patient->save();

        $this->info('Patient has been successfully created!');

        return 0;
    }
}
