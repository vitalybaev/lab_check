<?php

namespace App\Console\Commands;

use App\Models\Gemotest\GemotestResult;
use Illuminate\Console\Command;

class SendGemotestResultCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gemotest:send {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправляет результат';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $resultId = $this->argument('id');

        $gemotestResult = GemotestResult::findOrFail($resultId);
        if (!$gemotestResult->email) {
            $this->error('Не указан email');
        } else {
            $gemotestResult->sendEmail($gemotestResult->email);
        }

        return 0;
    }
}
