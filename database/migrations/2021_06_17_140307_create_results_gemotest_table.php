<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsGemotestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results_gemotest', function (Blueprint $table) {
            $table->id();
            $table->integer('patient_id')->unsigned()->index();
            $table->integer('patient_document_id')->unsigned()->index();
            $table->string('result_uid');
            $table->string('result_id');
            $table->timestamp('analysis_taken_at');
            $table->timestamp('analysis_ready_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results_gemotest');
    }
}
